function checkInput()
{
	var error = true;
	if(document.getElementById("name").value == "")
	{
		error = false;
		document.getElementById("error-name").innerHTML = "Chưa nhập tên";
		document.getElementById("div-error-name").style.display = 'block';
	}
	else
	{
		document.getElementById("div-error-name").style.display = 'none';
	}
	if(document.getElementById("category").value == "")
	{
		error = false;
		document.getElementById("error-category").innerHTML = "Chưa chọn loại";
		document.getElementById("div-error-cate").style.display = 'block';
	}
	else
	{
		document.getElementById("div-error-cate").style.display = 'none';
	}
	if(document.getElementById("price").value == "")
	{
		error = false;
		document.getElementById("error-price").innerHTML = "Chưa nhập giá";
		document.getElementById("div-error-price").style.display = 'block';
	}
	else
	{
		document.getElementById("div-error-price").style.display = 'none';
	}
	if(document.getElementById("rdoStatusYes").checked == false && document.getElementById("rdoStatusNo").checked == false)
	{
		error = false;
		document.getElementById("error-status").innerHTML = "Chưa chọn tình trạng";
		document.getElementById("div-error-status").style.display = 'block';
	}
	else
	{
		document.getElementById("div-error-status").style.display = 'none';
	}
	if(document.getElementById("des").value == "")
	{
		error = false;
		document.getElementById("error-des").innerHTML = "Chưa nhập miêu tả";
		document.getElementById("div-error-des").style.display = 'block';
	}
	else
	{
		document.getElementById("div-error-des").style.display = 'none';
	}
	return error;
}

function checkImageAdd()
{
	var error = true;
	if(document.getElementById("fImage").value == "")
	{
		error = false;
		document.getElementById("error-image").innerHTML = "Chưa chọn hình ảnh";
		document.getElementById("div-error-image").style.display = 'block';
	}
	else
	{
		if(document.getElementById("fImage").size > 2048)
		{
			error = false;
			document.getElementById("error-image").innerHTML = "Giới hạn kích thước file là 2048KB";
			document.getElementById("div-error-image").style.display = 'block';
		}
		else if((document.getElementById("image").width < 300) || (document.getElementById("image").height < 300))
		{
			error = false;
			document.getElementById("error-image").innerHTML = "Giới hạn kích cỡ ảnh là 300x300";
			document.getElementById("div-error-image").style.display = 'block';
		}
		else
		{
			document.getElementById("div-error-image").style.display = 'none';
		}
	}
	return error;
}

function checkImageEdit()
{
	var error = true;
	if(document.getElementById("fImage").size > 2048)
	{
		error = false;
		document.getElementById("error-image").innerHTML = "Giới hạn kích thước file là 2048KB";
		document.getElementById("div-error-image").style.display = 'block';
	}
	else if((document.getElementById("image").width < 300) || (document.getElementById("image").height < 300))
	{
		error = false;
		document.getElementById("error-image").innerHTML = "Giới hạn kích cỡ ảnh là 300x300";
		document.getElementById("div-error-image").style.display = 'block';
	}
	else
	{
		document.getElementById("div-error-image").style.display = 'none';
	}
	return error;
}

function loadImage(event)
{
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('image');
      output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
}

function checkProductAdd()
{
	var input = checkInput();
	var image = checkImageAdd();
	if(input == true && image == true)
	{
		return true;
	}
	return false;
}

function checkProductEdit()
{
	var input = checkInput();
	var image = checkImageEdit();
	if(input == true && image == true)
	{
		return true;
	}
	return false;
}

/*
function checkInput()
{
	var error = false;
	if(document.getElementById("name").value == "")
	{
		error = true;
		document.getElementById("error-name").innerHTML = "Chưa nhập tên";
	}
	else if(document.getElementById("category").value == "")
	{
		error = true;
		document.getElementById("error-category").innerHTML = "Chưa nhập loại";
	}
	else if(document.getElementById("price").value == "")
	{
		error = true;
		document.getElementById("error-price").innerHTML = "Chưa nhập giá";
	}
	else if(document.getElementById("status").value == "")
	{
		error = true;
		document.getElementById("error-status").innerHTML = "Chưa nhập tình trạng";
	}
	else if(document.getElementById("des").value == "")
	{
		error = true;
		document.getElementById("error-des").innerHTML = "Chưa nhập miêu tả";
	}
	else if(document.getElementById("fImage").value == "")
	{
		error = true;
		document.getElementById("error-image").innerHTML = "Chưa chọn hình ảnh";
	}
	if(error)
	{
		document.getElementById("div-error").style.display = 'block';
		return false;
	}
	return true;
}


function checkInput()
{
	var error_text = "";
	if(document.getElementById("name").value == "")
	{
		error_text = "Chưa nhập tên";
		//document.getElementById("error-name").innerHTML = "Chưa nhập tên";
	}
	else if(document.getElementById("category").value == "")
	{
		error_text = "Chưa nhập loại";
	}
	else if(document.getElementById("price").value == "")
	{
		error_text = "Chưa nhập giá";
	}
	else if(document.getElementById("status").value == "")
	{
		error_text = "Chưa nhập tình trạng";
	}
	else if(document.getElementById("des").value == "")
	{
		error_text = "Chưa nhập miêu tả";
	}
	else if(document.getElementById("fImage").value == "")
	{
		error_text = "Chưa chọn hình ảnh";
	}
	else if(document.getElementById("fImage").size > 2048)
	{
		error_text = "Giới hạn kích thước file là 2048KB";
	}
	else if((document.getElementById("image").width < 300) || (document.getElementById("image").height < 300))
	{
		error_text = "Giới hạn kích cỡ ảnh là 300x300";
	}
	if(error_text != "")
	{
		document.getElementById("error").innerHTML = error_text;
		document.getElementById("div-error").style.display = 'block';
		return false;
	}
	return true;
}
*/

