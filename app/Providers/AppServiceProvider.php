<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//Cần có thư viện này
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    //để sử dụng các thuộc tính unique,...
    public function boot()
    {
        Schema::defaultStringLength(200);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
