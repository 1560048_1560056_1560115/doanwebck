<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\ProductPost;
use App\Http\Requests\ProductEdit; 
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    //
    public function getCateList(){
        $ds_category = DB::select('select * from category');
        return view('admin.cate_list',['ds_category'=>$ds_category]);
    }

    public function getHome(){
    	return view('admin.home_admin');
    }

    public function getCateAdd(){
    	return view('admin.cate_add');
    }

    public function getCateEdit(){
    	return view('admin.cate_edit');
    }

    public function getProductList(){
        $ds_sanpham = DB::select('select * from product');
    	return view('admin.product_list',['ds_sanpham'=>$ds_sanpham]);
    }

    public function getProductAdd(){
        $cates = DB::select('select * from category');
    	return view('admin.product_add',['cates'=>$cates]);
    }

    public function getProductEdit($id){
        $sanpham = DB::select('Select * From product Where id=?',[$id]);
        $current = DB::select('Select * From category Where id=?',[$sanpham[0]->type]);
        $cates = DB::select('select * from category');
    	return view('admin.product_edit',['sanpham'=>$sanpham,'cates'=>$cates,'current'=>$current]);
    }

    public function getUserList(){
        $users =DB::table('users')->get();
    	return view('admin.user_list',['users'=>$users]);
    }

    public function getUserAdd(){
    	return view('admin.user_add');
    }

    public function getUserEdit($id){
        $user =DB::table('users')->where('id',$id)->first();
    	return view('admin.user_edit',['user'=>$user]);
    }

    public function postUserEdit(Request $request){
        if(!empty($request->txtPass)){
            $this->validate($request,['txtRePass'=>'required|same:txtPass']);
            DB::table('users')->where('id',$request->id)->update(['password'=>Hash::make($request->txtPass)]);
        }
        if(!empty($request->txtUserName)){
            DB::table('users')->where('id',$request->id)->update(['name'=>$request->txtUserName]);
        }

         DB::table('users')->where('id',$request->id)->update(['type'=>$request->rdoLevel]);
        
        return redirect()->route('user_list')->with(['status'=>'Sua doi Thanh Cong!','level'=>'success']);
        
    }

    public function getUserDelete($id){
        $user =DB::table('users')->where('id',$id)->first();
        if($user->status=='online'){
             return redirect()->back()->with(['status'=>'Tai khoan hien tai dang dung khong the xoa!','level'=>'danger']);
        }
        DB::table('users')->where('id',$id)->delete();
        return redirect()->back()->with(['status'=>'Xoa user thanh cong!','level'=>'success']);
    }

    public function postUserAdd(Request $request){

         
            $this->validate($request,[
            'txtUserName'=>'required|max:255|min:5',
            'txtEmail'=>'required|email|unique:users,email',
            'txtPass'=>'required|min:6|max:32',
            'txtRePass'=>'required|same:txtPass'
        ]);
            DB::table('users')->insert(['email' => $request->txtEmail,'name'=>$request->txtUserName, 'password' =>Hash::make( $request->txtPass),'type'=>$request->rdoLevel,'status'=>'offline']);
        

         
        
        return redirect()->route('user_list')->with(['status'=>'them user Thanh Cong!','level'=>'success']);
    }

    public function loginAdmin(){
        return view('admin.login');
    }

    public function postProductAdd(ProductPost $request){
        $lastRow = DB::table('product')->latest('id')->first();
        $lastRow = $lastRow->id + 1;
        if(!empty($request->file('fImage'))){
            $imageProduct = $request->file('fImage')->getClientOriginalName();
            $imageProduct = $lastRow."_".$imageProduct;
            $request->file('fImage')->move("home/images/",$imageProduct);
        }
        $image = new SimpleImage();
        $image->load("home/images/".$imageProduct);
        $image->resize(300,300);
        $image->save("home/images/".$imageProduct);
        DB::table('product')->insert([
            ['id'=> $lastRow,'name_product'=> $request->name,'price'=>$request->price,'on_sale'=>$request->rdoStatus,'description'=>$request->des,'image'=>"home/images/".$imageProduct,'views_number'=>$request->view,'type'=>$request->category]]);
        return redirect()->route('admin.list');
    }

    public function postProductEdit(ProductEdit $request,$id){
        $imageProduct = DB::table('product')->where('id',$id)->get();
        $imageProduct = $imageProduct[0]->image;
        if(!empty($request->file('fImage'))){
            if(file_exists($imageProduct))
                unlink($imageProduct);
            $imageProduct = $request->file('fImage')->getClientOriginalName();
            $imageProduct = $id."_".$imageProduct;
            $request->file('fImage')->move("home/images/",$imageProduct);
            $imageProduct = "home/images/".$imageProduct;
            $image = new SimpleImage();
            $image->load($imageProduct);
            $image->resize(300,300);
            $image->save($imageProduct);
        }
        DB::table('product')->where('id',$id)->update(
            ['name_product'=> $request->name,'price'=>$request->price,'on_sale'=>$request->rdoStatus,'description'=>$request->des,'image'=>$imageProduct,'views_number'=>$request->view,'type'=>$request->category]);
        return redirect()->route('admin.list');
    }

    public function getDeleteProduct($id){
        $imageProduct = DB::table('product')->where('id',$id)->get();
        $imageProduct = $imageProduct[0]->image;
        if(file_exists($imageProduct))
            unlink($imageProduct);
        DB::table('product')->where('id',$id)->delete();
        DB::table('review')->where('id_product',$id)->delete();
        return redirect()->route('admin.list');
    } 
}

?>

<?php
 
class SimpleImage {
            var $image;
            var $image_type; 
            function load($filename) {

        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
        $this->image = imagecreatefromjpeg($filename);
        }elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        }elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }
 
function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75,$permissions=null) {
      if( $image_type == IMAGETYPE_JPEG ) {
    imagejpeg($this->image,$filename,$compression);
    }elseif( $image_type == IMAGETYPE_GIF ) {
    imagegif($this->image,$filename);
    }elseif( $image_type == IMAGETYPE_PNG ) {
    imagepng($this->image,$filename);
    }
    if( $permissions != null) {
            chmod($filename,$permissions);
    }
}
 
function output($image_type=IMAGETYPE_JPEG) {
    if( $image_type == IMAGETYPE_JPEG ) { 
        imagejpeg($this->image);
    }elseif( $image_type == IMAGETYPE_GIF ) {
        imagegif($this->image);
    }elseif( $image_type == IMAGETYPE_PNG ) {
        imagepng($this->image);
    }
}
 
function getWidth() {
    return imagesx($this->image);
}
 
function getHeight() {
    return imagesy($this->image);
}
 
function resizeToHeight($height) {
    $ratio = $height / $this->getHeight();
    $width = $this->getWidth() * $ratio;
    $this->resize($width,$height);
}
 
function resizeToWidth($width) {
    $ratio = $width / $this->getWidth();
    $height = $this->getheight() * $ratio;
    $this->resize($width,$height);
}

function scale($scale) {
    $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
    $this->resize($width,$height);
}

function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height); 
    imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
}     
 
}
 
?>