<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileEdit;
use Illuminate\Support\Facades\DB;
use Cart;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Hash;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('home.home');
    }

    public function shop()
    {
        //$ds_sanpham = DB::select('select * from product');
        //return view('home.shop',["ds_sanpham"=>$ds_sanpham]);
        //$ds_sanpham = DB::select('Select * From product');
        $ds_sanpham = DB::table('product')->paginate(8);
        return view('home.shop',["ds_sanpham"=>$ds_sanpham]);   
    }

    public function Search(Request $request)
    {
        $search = $request->search;
        $ds_sanpham = Product::select('id','name_product','price','description','image','on_sale','views_number','type')->search($search)->paginate(6);
        return view('home.search',["ds_sanpham"=>$ds_sanpham]);  
    }

    public function login()
    {
        return view('home.log-in');
    }

    public function signup()
    {
        return view('home.sign-up');
    }

    public function aboutus()
    {
        return view('home.about-us');
    }

    public function contactus()
    {
        return view('home.contact-us');
    }

    public function shop_product($id)
    {
        $ds_sanpham = DB::select('select * from product where id=?',[$id]);
        //$ds_review = DB::select('select * from review where id_product=?',[$id]);
        //$ds_review = DB::table('review')->where('id_product',$id)->paginate(3);
        //$ds_review_ = DB::table('review')->where('id_product',$id)->join('users','review.email','<>','users.email')->select('review.*')->paginate(3);
        $ds_review = DB::table('review')->leftjoin('users','review.email','=','users.email')->where('review.id_product',$id)->select('review.*','users.image')->paginate(3);
        $the_loai = DB::select('select * from category where id=?',[$ds_sanpham[0]->type]);
        return view('home.shop_product',["ds_sanpham"=>$ds_sanpham,"ds_review"=>$ds_review,"the_loai"=>$the_loai]);
    }

    public function shop_category($id)
    {
        $ds_sanpham = DB::table('product')->where('type',$id)->paginate(2);
        //$ds_sanpham = DB::select('select * from product where type=?',[$id]);
        $the_loai = DB::select('select * from category where id=?',[$id]);
        return view('home.shop_category',["ds_sanpham"=>$ds_sanpham,"the_loai"=>$the_loai]);
    }
    

     public function buyProduct($id){
        $product=DB::table('product')->where('id',$id)->first();
        Cart::add(array('id'=>$id,'name'=>$product->name_product,'price'=>$product->price,'quantity'=>1,'attributes'=> array('image'=>$product->image)));
       // $x= Cart::get(8);
        //return $x->attributes->image;
        return redirect()->back();
    }

    public function getbuyProduct(Request $request,$id){
        Cart::get($id)->quantity=$request->quantity;
       // $x= Cart::get(8);
        //return $x->attributes->image;
        return redirect()->back();
    }
    
    public function removeBuyProduct($id){
        Cart::remove($id);
       return redirect()->back();
    }

    public function getCartCustom(){
        return view('home.CartCustom');
    }

    public function postCommentGuest(Request $request,$id)
    {
        $checkEmail = null;
        $checkEmail = DB::table('review')->where('email',$request->email)->first();
        if($checkEmail === null)
        {
            DB::table('review')->insert([
                ['id_product'=>$id,'text_review'=>$request->comment,'name'=>$request->author,'email'=>$request->email]]);
            return redirect()->back();
        }
        else
        {
            return redirect()->back()->with('status', 'Email đã tồn tại');
        }
        //DB::table('review')->insert([
           // ['id_product'=>$id,'text_review'=>$request->comment,'name'=>$request->author,'email'=>$request->email]]);
        //return redirect()->back();
    }

    public function postCommentUser(Request $request,$id)
    {
        DB::table('review')->insert([
            ['id_product'=>$id,'text_review'=>$request->comment,'name'=>Auth::user()->name,'email'=>Auth::user()->email]]);
        return redirect()->back();
    }


    public function profile($id)
    {
        $info = DB::select('select * from users where id=?',[$id]);
        return view('home.profile',['info'=>$info]);
    }

    public function getUpdateBuyProduct(Request $request){
        foreach (Cart::getContent() as $product ) {
            $product->quantity=$request->get("quantity$product->id");
        }
        return redirect()->back();  
    }

    public function getCheckOut(){
        return view('home.CheckOut');
    }

    public function postCheckOut(){
        return view('home.CheckOut');
    }

    public function getSaveBillSuccess()
    {
        return view('home.savebillsuccess');
    }

    public function postSaveCheckOut(request $request){
        $this->validate($request,[
            'billing_first_name'=>'required|min:2',
            'billing_last_name'=>'required|min:2',
            'billing_email'=>'required|email',
            'billing_address_1'=>'required',
            'billing_country'=>'required',
            'billing_city'=>'required',
            'billing_phone'=>'required|regex:/[0-9]{9}/|min:10|max:11',
            
        ]/*,[
            'billing_first_name.required'=>'Ban chua nhap first name',
            'billing_last_name.required'=>'Ban chua nhap last name',
            'billing_email.required'=>'Ban chua nhap email',
            'billing_email.email'=>'Ban chua nhap dung dinh dang email',
            'billing_address_1.required'=>'Ban chua nhap dia chi',
            'billing_country.required'=>'Ban chua chon country',
            'billing_city.required'=>'Ban chua chon city',
            'billing_phone.required'=>'Ban chua nhap so dien thoai',
            'billing_phone.min'=>'so dien thoai toi thieu la 10 so',
            'billing_phone.max'=>'so dien thoai toi da la 11 so',
            'billing_phone.numeric'=>'yeu cau so dien thoai phai la so',    
            

        ]*/);



       
        if(!Cart::isEmpty()){
        $idcus=1;
        $customers=DB::table('customer')->orderBy('idcustomer')->get();

        foreach ($customers as $customer) {
            
            if($customer->idcustomer!=$idcus){
                break;
            }
            echo $customer->idcustomer .'<br>';
            $idcus++;
        }

        DB::table('customer')->insert([['idcustomer'=>$idcus,'firstname'=>$request->billing_first_name,'lastname'=>$request->billing_last_name,'companyname'=>$request->billing_company,'idCountry'=>$request->billing_country,'streetaddress'=>$request->billing_address_1,'town/city'=>$request->billing_city,'phone'=>$request->billing_phone,'email'=>$request->billing_email]]);

         $idb=1;
        $bills=DB::table('bill')->orderBy('idbill')->get();
        foreach ($bills as $bill) {
            if($bill->idbill!=$idb){
                break;
            }
            $idb++;
        }

        DB::table('bill')->insert([['idbill'=>$idb,'idcustomer'=>$idcus,'dateorder'=>date('Y-m-d'),'total'=>Cart::getTotal(),'note'=>$request->order_comments]]);
        foreach (Cart::getContent() as $product) {
            DB::table('billdetail')->insert([['idbill'=>$idb,'idproduct'=>$product->id,'quantity'=>$product->quantity,'unitprice'=>$product->price/$product->quantity]]);
        }
            Cart::clear();
            return redirect()->route('savebillsuccess');
        }
        else{
            return redirect('shop');
        }
    }

    public function getlogout(){

        DB::table('users')->where('id',Auth::user()->id)->update(['status'=>'offline']);

        Auth::logout();

        return redirect('/');
    }

    public function postProfileEdit(ProfileEdit $request,$id)
    {
        $user = DB::table('users')->where('id',$id)->get();
        $checkEmail = DB::table('users')->where('email',$request->email)->first();
        if($checkEmail === null or $user[0]->email === $request->email)
        {
            $imageUser = DB::table('users')->where('id',$id)->get();
            $imageUser = $imageUser[0]->image;
            if(!empty($request->file('fImage'))){
                if(file_exists($imageUser))
                    unlink($imageUser);
                $imageUser = $request->file('fImage')->getClientOriginalName();
                $imageUser = $id."_".$imageUser;
                $request->file('fImage')->move("home/images/",$imageUser);
                $imageUser = "home/images/".$imageUser;
            }
            DB::table('users')->where('id',$id)->update(['name'=>$request->name,'email'=>$request->email,'image'=>$imageUser]);
            return redirect()->back();
        }
        else
        {
            return redirect()->back()->with('status', 'Email đã tồn tại');
        }
    }

    public function changePass($id)
    {
        $info = DB::select('select * from users where id=?',[$id]);
        return view('home.change_pass',['info'=>$info]);
    }

    public function postChangePass(Request $request,$id)
    {
            $this->validate($request,[
                'current_pass'=>'required|min:6|max:50',
                'new_pass'=>'required|min:6|max:50|',
                'confi_pass'=>'required|same:new_pass']);
        $user = DB::table('users')->where('id',$id)->first();
        if(Hash::check($request->current_pass, $user->password))
        {
            DB::table('users')->where('id',$id)->update(['password'=>Hash::make($request->new_pass)]);
           return redirect()->back()->with('statusSuccess', 'Change password success');;
        }
        else
        {
            return redirect()->back()->with('status', 'Sai password');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

?>

<?php
 
class SimpleImage {
            var $image;
            var $image_type; 
            function load($filename) {

        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
        $this->image = imagecreatefromjpeg($filename);
        }elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        }elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }
 
function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75,$permissions=null) {
      if( $image_type == IMAGETYPE_JPEG ) {
    imagejpeg($this->image,$filename,$compression);
    }elseif( $image_type == IMAGETYPE_GIF ) {
    imagegif($this->image,$filename);
    }elseif( $image_type == IMAGETYPE_PNG ) {
    imagepng($this->image,$filename);
    }
    if( $permissions != null) {
            chmod($filename,$permissions);
    }
}
 
function output($image_type=IMAGETYPE_JPEG) {
    if( $image_type == IMAGETYPE_JPEG ) { 
        imagejpeg($this->image);
    }elseif( $image_type == IMAGETYPE_GIF ) {
        imagegif($this->image);
    }elseif( $image_type == IMAGETYPE_PNG ) {
        imagepng($this->image);
    }
}
 
function getWidth() {
    return imagesx($this->image);
}
 
function getHeight() {
    return imagesy($this->image);
}
 
function resizeToHeight($height) {
    $ratio = $height / $this->getHeight();
    $width = $this->getWidth() * $ratio;
    $this->resize($width,$height);
}
 
function resizeToWidth($width) {
    $ratio = $width / $this->getWidth();
    $height = $this->getheight() * $ratio;
    $this->resize($width,$height);
}

function scale($scale) {
    $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
    $this->resize($width,$height);
}

function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height); 
    imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
}     
 
}
 
?>
