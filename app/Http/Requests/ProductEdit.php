<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'price' => 'required',
            'rdoStatus' => 'required',
            'des' => 'required',
            'fImage' => 'dimensions:min_width=300,min_height=300|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
            'name.max' => "Độ dài tối đa là 255",
            'price.required' => 'Chưa nhập giá',
            'rdoStatus.required' => 'Chưa nhập tình trạng',
            'des.required' => 'Chưa nhập miêu tả',
            'fImage.dimensions' => 'Kích cỡ tối thiểu là 300x300',
            'fImage.max' => 'Kích thước file tối đa là 2048KB'
        ];
    }
}
