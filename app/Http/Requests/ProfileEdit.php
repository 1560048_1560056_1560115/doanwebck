<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'fImage' => 'dimensions:min_width=50,min_height=50|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
            'email.required' => 'Chưa nhập email',
            'email.email' => 'Email không hợp lệ',
            'fImage.dimensions' => 'Kích cỡ tối thiểu là 50x50',
            'fImage.max' => 'Kích thước file tối đa là 2048KB'
        ];
    }
}
