<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    protected $fillable = ['name_product','price','description','image','on_sale','views_number','type'];

    public function scopeSearch($query, $s){
        return $query->where('name_product','like','%'.$s.'%');
    }
}
