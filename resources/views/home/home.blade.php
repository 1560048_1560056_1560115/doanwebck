@extends('home.layouts.master')
@section('css')
	<link rel='stylesheet' href='home/js/vendor/revslider/rs-plugin/css/settings.css' type='text/css' media='all' />
@endsection

@section('content')


			<div id="mainslider_1" class="sliderHeader staticSlider slider_engine_revo mainslider_1">
				<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
					<div id="rev_slider_1_1" class="rev_slider fullwidthabanner">
						<ul>
							<!-- SLIDE  -->
							<li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
								<!-- MAIN IMAGE -->
								<img src="home/js/vendor/revslider/rs-plugin/images/transparent.png"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->
								<!-- LAYER NR. 1 -->
								<div class="tp-caption tp-fade"
									data-x="-51"
									data-y="center" data-voffset="-71"
									data-speed="300"
									data-start="500"
									data-easing="Power3.easeInOut"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-endspeed="300"
									>
									<img src="home/images/large_1.jpg" alt="">
								</div>
								<!-- LAYER NR. 2 -->
								<div class="tp-caption black tp-fade tp-resizeme"
									data-x="125"
									data-y="center" data-voffset="0"
									data-speed="300"
									data-start="500"
									data-easing="Power3.easeInOut"
									data-splitin="none"
									data-splitout="none"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-endspeed="300"
									>
									<span class="rs_text_style_1">A huge variety<br/>  of the freshest fruits & vegetables.</span>
								</div>
							</li>
							<!-- SLIDE  -->
							<li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
								<!-- MAIN IMAGE -->
								<img src="home/js/vendor/revslider/rs-plugin/images/transparent.png"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->
								<!-- LAYER NR. 1 -->
								<div class="tp-caption tp-fade"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="100"
									data-speed="300"
									data-start="500"
									data-easing="Power3.easeInOut"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-endspeed="300"
									>
									<img src="home/images/large_2.jpg" alt="" data-ww="1900" data-hh="1257">
								</div>
								<!-- LAYER NR. 2 -->
								<div class="tp-caption black xs_position_1 tp-fade tp-resizeme"
									data-x="right" data-hoffset="-294"
									data-y="center" data-voffset="-46"
									data-speed="300"
									data-start="500"
									data-easing="Power3.easeInOut"
									data-splitin="none"
									data-splitout="none"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-endspeed="300"
									>
									<h1 class="rs_text_color_1">Try Organic Food</h1>
								</div>
								<!-- LAYER NR. 3 -->
								<div class="tp-caption black tp-fade tp-resizeme"
									data-x="right" data-hoffset="-200"
									data-y="center" data-voffset="40"
									data-speed="300"
									data-start="500"
									data-easing="Power3.easeInOut"
									data-splitin="none"
									data-splitout="none"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-endspeed="300"
									>
									<span class="rs_text_style_1">...or as your grandparents  called it, </span>
									<h3 class="rs_text_color_1">"Food"</h3>
								</div>
							</li>
							<!-- SLIDE  -->
							<li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
								<!-- MAIN IMAGE -->
								<img src="home/js/vendor/revslider/rs-plugin/images/transparent.png"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->
								<!-- LAYER NR. 1 -->
								<div class="tp-caption tp-fade"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="0"
									data-speed="300"
									data-start="500"
									data-easing="Power3.easeInOut"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-endspeed="300"
									>
									<img src="home/images/large_3.jpg" alt="" data-ww="1890" data-hh="1260">
								</div>
							</li>
						</ul>
						<div class="tp-bannertimer"></div>
					</div>
				</div>
			</div>

			<div class="wrapContent">
				<div id="wrapWide" class="wrapWide">
					<div class="content">
						<section class="singlePage emptyPostFormatIcon emptyPostTitle emptyPostInfo page">
							<article class="postContent">
								<div class="postTextArea">

									<section class="">
										<div class="container-fluid">
											<div class="sc_blogger sc_blogger_horizontal style_portfolio_mini portfolioWrap sc_blogger_indent">
												<div class="masonryWrap">
													<section class="masonryStyle isotopeWrap portfolio_mini" data-foliosize="300">
														<article class="isotopeItem post_format_standard isw_1 odd" data-postid="3158" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block10.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_1.jpg" alt="In Harmony With Nature">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		In Harmony With Nature</h4>
																		<div class="isotopeExcerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 even" data-postid="3155" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block11.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_2.jpg" alt="Go Organic With Our Farm!">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		Go Organic With Our Farm!</h4>
																		<div class="isotopeExcerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 odd" data-postid="3152" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block12.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_3.jpg" alt="Food Allergy Survival">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		Food Allergy Survival</h4>
																		<div class="isotopeExcerpt">Sed ut perspiciatis, unde omnis iste natus error sit voluptatem...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 even" data-postid="3149" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block13.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_4.jpg" alt="Healthy &#038; Happy">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		Healthy &#038; Happy</h4>
																		<div class="isotopeExcerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 odd" data-postid="3146" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block14.html" class="isotopeItemWrap">
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeStatickWrap">
																	<div class="isotopeStatick">
																		<div class="postFormatIcon icon-post">
																		</div>
																		<div class="isotopeTags">
																			<a class="tag_link" href="#">Brands</a>
																			<a class="tag_link" href="#">Organic</a>
																		</div>
																		<h3 class="isotopeTitle lower">
																		<a href="#">Exclusive brands</a>
																		</h3>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																		<div class="isotopeExcerpt">Our criteria included recommendations taken from The Cornucopia Institute Scorecards (soy and carrageenan). As well, we looked at the operation of...</div>
																		<a href="#" class="isotopeReadMore">read more</a>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 even" data-postid="3143" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block15.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_5.jpg" alt="Green &#038; Sustainable">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		Green &#038; Sustainable</h4>
																		<div class="isotopeExcerpt">Sed ut perspiciatis, unde omnis iste natus error sit voluptatem...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 odd" data-postid="3123" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block16.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_6.jpg" alt="Why Farm Organically?">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		Why Farm Organically?</h4>
																		<div class="isotopeExcerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 even" data-postid="3117" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block17.html" class="isotopeItemWrap">
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeStatickWrap">
																	<div class="isotopeStatick">
																		<div class="postFormatIcon icon-post">
																		</div>
																		<div class="isotopeTags">
																			<a class="tag_link" href="#">New</a>
																			<a class="tag_link" href="#">Organic</a>
																		</div>
																		<h3 class="isotopeTitle lower">
																		<a href="#">New products</a>
																		</h3>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																		<div class="isotopeExcerpt">Poor nutrition is a chronic problem often linked to poverty, poor nutrition understanding and practices, and deficient sanitation and food...</div>
																		<a href="#" class="isotopeReadMore">read more</a>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 odd" data-postid="3115" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block18.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_7.jpg" alt="It&#8217;s All About Veggies">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		It&#8217;s All About Veggies</h4>
																		<div class="isotopeExcerpt">Itaque earum rerum hic tenetur a sapiente delectus, ut aut...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 even" data-postid="3111" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block19.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_8.jpg" alt="Our Cereal Production">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		Our Cereal Production</h4>
																		<div class="isotopeExcerpt">Sed ut perspiciatis, unde omnis iste natus error sit voluptatem...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 odd" data-postid="3109" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block20.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_9.jpg" alt="What&#8217;s On the Plate?">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		What&#8217;s On the Plate?</h4>
																		<div class="isotopeExcerpt">Sed blandit, ante vitae blandit rutrum sapien nisl ornare nibh nec...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
														<article class="isotopeItem post_format_standard isw_1 even last" data-postid="3105" data-wdh="620" data-hgt="620" data-incw="1" data-inch="1">
															<div data-url="home/ajax/block21.html" class="isotopeItemWrap">
																<div class="thumb">
																	<img src="home/images/medium_10.jpg" alt="Lactose Intolerant? Try Our Organic Milk">
																</div>
																<div class="isotopeMore icon-down-open-big"></div>
																<div class="isotopeContentWrap">
																	<div class="isotopeContent">
																		<h4 class="isotopeTitle">
																		Lactose Intolerant? Try Our...</h4>
																		<div class="isotopeExcerpt">Sed ut perspiciatis, unde omnis iste natus error sit voluptatem...</div>
																		<div class="postInfo hoverUnderline">
																			<div class="postWrap"></div>
																		</div>
																	</div>
																</div>
															</div>
														</article>
													</section>
												</div>
											</div>
										</div>
									</section>

								</div>
							</article>
						</section>
					</div>
				</div>
			</div>

@endsection

@section('sidebar')
@endsection

@section('script')
	<script type='text/javascript' src='home/js/vendor/revslider/rs-plugin/js/jquery.themepunch.tools.min.js'></script>
	<script type='text/javascript' src='home/js/vendor/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
@endsection