@extends('home.layouts.master')
@section('css')
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/select2.css" type='text/css' media='all' />
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/woocommerce-layout.css" type='text/css' media='all' />
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/woocommerce-smallscreen.css" type='text/css' media='only screen and (max-width: 768px)' />
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/woocommerce.css" type='text/css' media='all' />
	<link rel='stylesheet' href="/home/css/woo-style.css" type='text/css' media='all' />
@endsection

@section('body')
<body class="woocommerce woocommerce-page">

	<div id="wrap" class="wrap sideBarRight sideBarShow menuStyle1 menuSmartScrollShow blogStyleExcerpt bodyStyleWide menuStyleFixed visibleMenuDisplay logoImageStyle logoStyleBG">
@endsection

@section('menu_cart')
							<li class="usermenuCart">
								
								<a href="#" class="cart_button">
									<img src="/home/images/icon-cart.png"  alt="">
								</a>
								<ul class="widget_area sidebar_cart sidebar">
									<li>
										<div class="widget woocommerce widget_shopping_cart">
											<div class="hide_cart_widget_if_empty">
												<div class="widget_shopping_cart_content" style="opacity: 1;">
													@if(!Cart::isEmpty())
													<ul class="woocommerce-mini-cart cart_list product_list_widget  sf-js-enabled">
														@foreach(Cart::getContent() as $product)
														<li class="woocommerce-mini-cart-item mini_cart_item">
																	
															<a href="{{route('detail',$product->id)}}">
																<img width="180" height="180" src="/{{$product->attributes->image}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="/{{($product->attributes->image)}} 180w" sizes="(max-width: 180px) 100vw, 180px">{{$product->name}}&nbsp;                </a> 	
												
															<span class="quantity">{{$product->quantity}} × 
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">£</span>{{$product->price}}
																</span>
															</span>		

															<a href="{{route('remove_buy_product',$product->id)}}" class="remove remove_from_cart_button " aria-label="Remove this item" data-product_id="{{$product->id}}" data-cart_item_key="0cbed40c0d920b94126eaf5e707be1f5" data-product_sku="" style="position:absolute; left: 250px;" >×</a>

														</li>
															@endforeach
													</ul>
													<p class="woocommerce-mini-cart__total total"><strong>Subtotal:</strong> <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>5.00</span></p>
													<p class="woocommerce-mini-cart__buttons buttons"><a href="{{route('cartcustom')}}" class="button wc-forward">View cart</a><a href="{{route('getcheckout')}}" class="button checkout wc-forward">Checkout</a></p>
													@else
													<ul class="cart_list product_list_widget">
														
													<li class="empty">No products in the cart.</li>
													
													</ul>
													@endif
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>
@endsection

@section('content')

			<div class="wrapContent">
				<div id="wrapWide" class="wrapWide">
					<div class="content">

						<section class="">
							<div class="container">
								<section class="post shop_mode_thumbs">
									<article class="post_content">
										<nav class="woocommerce-breadcrumb">
											<a href="/">Home</a>&nbsp;&#47;&nbsp;
											<a href="{!! URL::route('category',$the_loai[0]->id) !!}">{{$the_loai[0]->name}}</a>&nbsp;&#47;&nbsp;
										</nav>

										<div class="product">

											@if ($ds_sanpham[0]->on_sale == 1)
											<span class="onsale">Sale!</span>
											@endif
											<div class="images">
												<a href="/{{$ds_sanpham[0]->image}}" class="woocommerce-main-image zoom" title="">
													<img src="/{{$ds_sanpham[0]->image}}" class="attachment-shop_single" alt="{{$ds_sanpham[0]->name_product}}" title="{{$ds_sanpham[0]->name_product}}" />
												</a>
											</div>

											<div class="summary entry-summary">
												<h1 class="product_title entry-title">{{$ds_sanpham[0]->name_product}}</h1>
												<div>
													<p class="price">
													<span class="amount">Price: </span>
														@if ($ds_sanpham[0]->on_sale == 1)
															<del>
															<span class="amount">&pound;15.00</span>
															</del>
														@endif
														<ins>
														<span class="amount">&pound;{{$ds_sanpham[0]->price}}</span>
														</ins>
														<p>Views: {{$ds_sanpham[0]->views_number}}</p>
													</p>
												</div>
												<form class="cart" method="get" action="{{route('getbuyproduct',$ds_sanpham[0]->id)}}" enctype='multipart/form-data'>
													<div class="quantity">
														@if(!Cart::isEmpty())
														<input type="number" step="1" min=""  name="quantity" value="{{Cart::get($ds_sanpham[0]->id)->quantity}}" title="Qty" class="input-text qty text" size="4" />
														@else
														<input type="number" step="1" min="1"  name="quantity" value="1" title="Qty" class="input-text qty text" size="4" />
														@endif

													</div>
													<input type="hidden" name="add-to-cart" value="4015" />
													
													<button type="submit" class="single_add_to_cart_button button alt">Add to cart</button>
												</form>
												<div class="product_meta">
													<span class="posted_in">
														Categories: 
														<a href="{!! URL::route('category',$the_loai[0]->id) !!}" rel="tag">{{$the_loai[0]->name}}</a> 
													</span>
													<span class="product_id">Product ID:
														<span>{{$ds_sanpham[0]->id}}</span>
													</span>
												</div>
											</div>


											<div class="woocommerce-tabs wc-tabs-wrapper sc_tabs">
												<ul class="tabs wc-tabs sc_tabs_titles">
													<li class="tab_names description_tab">
														<a href="#tab-description">Description</a>
													</li>
													<li class="tab_names reviews_tab">
														<a href="#tab-reviews">Reviews</a>
													</li>
												</ul>
												<div class="panel entry-content wc-tab" id="tab-description">
													<h2>Product Description</h2>
													<div class="sc_section  sc_float_right sc_columns_1_2 margin_bottom_0">
														<div class="sc_skills  sc_skills_bar sc_skills_horizontal" data-type="bar" data-dir="horizontal">
															<div class="sc_skills_item sc_skills_style_1 odd first">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="98" data-step="1" data-max="100" data-speed="19" data-duration="1862" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">Pluses Item</div>
															<div class="sc_skills_item sc_skills_style_1 even">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="26" data-step="1" data-max="100" data-speed="39" data-duration="1014" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">Cons Item</div>
															<div class="sc_skills_item sc_skills_style_1 odd">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="87" data-step="1" data-max="100" data-speed="25" data-duration="2175" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">User Ratings</div>
														</div>
													</div>
													<p>{{$ds_sanpham[0]->description}}</p>
												</div>
												<div class="panel entry-content wc-tab" id="tab-reviews">
													<div id="reviews">
														<div id="comments">
															<h2>Reviews</h2>
														</div>
														<div id="review_form_wrapper">
														@foreach($ds_review as $review)
														<section class="author vcard with_border_top">
															<div class="container-fluid">
																<div class="authorInfo">
																	<div class="authorAva">
																		<a href="#" >
																			@if(is_null($review->image))
																			<img src='/home/images/user.png' class='avatar avatar-50 photo' />
																			@else
																			<img src='/{{$review->image}}' class='avatar avatar-50 photo' />
																			@endif
																		</a>
																	</div>
																	<div class="authorTitle hoverUnderline">Written by <a href="#">{{$review->name}}</a>
																</div>
																<div class="authorDescription hoverUnderline">{{$review->text_review}}</div>
																</div>
															</div>
														</section>
														@endforeach
														<nav class="woocommerce-pagination">
														<ul class='page-numbers'>
															<li>
																<span class='page-numbers'>{{ $ds_review->links() }}</span>
															</li>
														</ul>
														</nav>
															<div id="review_form">
																<div id="respond" class="comment-respond">
																	<h3 id="reply-title" class="comment-reply-title">Review “{{$ds_sanpham[0]->name_product}}”</h3>
																	@guest
																	<form action="{{route('post_comment_guest',[$ds_sanpham[0]->id])}}" method="post" id="commentform" class="comment-form">
								                        			<input type="hidden" = name="_token" value = "{!!csrf_token()!!}">
																		<p class="comment-form-author">
																			<label for="author">
																				Name 
																				<span class="required">*</span>
																			</label>
																			<input id="author" name="author" type="text" value="" size="30" aria-required="true" />
																		</p>
																		<p class="comment-form-email">
																			<label for="email">
																				Email 
																				<span class="required">*</span>
																			</label>
																			<input id="email" name="email" type="text" value="" size="30" aria-required="true" />
																		</p>
																		@if (session('status'))
																		<div class="sc_infobox sc_infobox_style_warning sc_infobox_closeable sc_infobox_horizontal">
																			<h4 class="sc_infobox_title">Warning</h4>
																			<span class="sc_infobox_line"></span>
																			<span class="sc_infobox_content">{{ session('status') }}</span>
																		</div>
																		@endif
																		<p class="comment-form-comment">
																			<label for="comment">Your Review</label>
																			<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
																		</p>
																		<p class="form-submit">
																			<input name="submit" type="submit" id="submit" class="submit" value="Submit" />
																			<input type='hidden' name='comment_post_ID' value='4015' id='comment_post_ID' />
																			<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
																		</p>
																		</form>
																	@else
																		<form action="{{route('post_comment_user',[$ds_sanpham[0]->id])}}" method="post" id="commentform" class="comment-form">
								                        				<input type="hidden" = name="_token" value = "{!!csrf_token()!!}">
																		<p class="comment-form-comment">
																			<label for="comment">Your Review</label>
																			<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
																		</p>
																		<p class="form-submit">
																			<input name="submit" type="submit" id="submit" class="submit" value="Submit" />
																			<input type='hidden' name='comment_post_ID' value='4015' id='comment_post_ID' />
																			<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
																		</p>
																		</form>
																	@endguest
																</div>
															</div>
														</div>
														<div class="clear"></div>
													</div>
												</div>
											</div>


											<!-- <div class="related products">
												<h2>Related Products</h2>
												<ul class="products">
													<li class="first product">
														<a href="#">
															<img src="/home/images/product_1.jpg" class="attachment-shop_catalog" alt="15532101040" />
															<h3>Delallo Pasta</h3>
															<span class="price">
																<span class="amount">&pound;3.65</span>
															</span>
														</a>
														<a href="#" class="button add_to_cart_button product_type_simple">Add to cart</a>
													</li>
													<li class="product">
														<a href="#">
															<img src="/home/images/product_9.jpg" class="attachment-shop_catalog" alt="apetito2" />
															<h3>Organic Capellini</h3>
															<span class="price">
																<span class="amount">&pound;5.00</span>
															</span>
														</a>
														<a href="#" class="button add_to_cart_button product_type_simple">Add to cart</a>
													</li>
												</ul>
											</div>
										</div> -->

									</article>
								</section>
							</div>
						</section>

					</div>

@endsection
