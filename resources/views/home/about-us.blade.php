@extends('home.layouts.master')

@section('content')

			<div class="wrapContent">
				<div id="wrapWide" class="wrapWide">
					<div class="content">
						<section class="singlePage emptyPostFormatIcon emptyPostTitle emptyPostInfo">
							<article class="postContent">
								<div class="postTextArea">

									<div class="sc_content mainWrap">
										
										<section class="">
											<div class="container-fluid">
												<div class=" sc_columns  sc_columns_2 sc_columns_indent">
													<div class=" sc_columns_item  sc_columns_item_coun_1 odd first" >
														<div  class="sc_image alignleft">
															<a href="home/images/place_click.jpg">
																<img  src="home/images/place.jpg" alt="" />
															</a>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_2 even" >
														<h2 class="sc_title sc_title_style_3 margin_bottom_25">Our Company’s Story</h2>
														<div class="margin_bottom_50">
															<p><strong>Established in 1982</strong>, our Farm is a healthy community located in Adamswille. We live a peaceful, shared life and believe in healthy body and mind. Browse our site to learn more of our history. Look through our online store to find fresh treats from our organic bakery, organic coffee and chocolate, as well as fresh dairy. We welcome you to come visit us! Just drop us few lines when you’d like to come.</p>
														</div>
														<h2 class="sc_title sc_title_style_3 margin_bottom_25">Why choose Healthy Farm</h2>
														<ul class="sc_list  sc_list_style_ul sc_list_style_1">
															<li class="sc_list_item  odd first sc_list_marked_no">
																<span>
																	Foods right form the patch. 12 certified organic farmers
																</span>
															</li>
															<li class="sc_list_item  even sc_list_marked_no">
																<span>
																	Non-GMO dairy products. Environmental production
																</span>
															</li>
															<li class="sc_list_item  odd sc_list_marked_no">
																<span>
																	High pressure meat processing. Humane &amp; sustainable farming
																</span>
															</li>
															<li class="sc_list_item  even sc_list_marked_no">
																<span>
																	Organically produced pork and beef. Caring of health of our cusomters
																</span>
															</li>
															<li class="sc_list_item  odd sc_list_marked_no">
																<span>
																	Organically grown fruit &amp; veggies. Best tasting product on the marke
																</span>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</section>

										<section class="section_padding_top_70 with_border_bottom">
											<div class="container-fluid">
												<h2 class="sc_title sc_title_normal sc_title_center margin_bottom_60">
													some of our products
												</h2>
												<div class="sc_columns sc_columns_3 sc_columns_indent margin_bottom_30">
													<div class=" sc_columns_item sc_columns_item_coun_1 odd first">
														<div class="sc_section sc_section_style_1">
															<div  class="sc_image sc_image_style_1 margin_bottom_40">
																<img  src="home/images/car.png" alt="" />
															</div>
															<h6 class="sc_title sc_title_style_2 sc_title_center">best quality products</h6>
															<div class="sc_text sc_text_style_1">
																<p>“Juices of fruits and vegetables are pure gifts from Mother Nature and the most natural way to heal your body.”</p>
															</div>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_2 colspan_2 even with_overflow" >
														<div class="sc_skills  sc_skills_bar sc_skills_horizontal" data-type="bar" data-dir="horizontal">
															<div class="sc_skills_item sc_skills_style_1 odd first">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="55" data-step="1" data-max="100" data-speed="15" data-duration="825" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">Organic cereals</div>
															<div class="sc_skills_item sc_skills_style_1 even">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="100" data-step="1" data-max="100" data-speed="16" data-duration="1600" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">nuts &amp; seeds</div>
															<div class="sc_skills_item sc_skills_style_1 odd">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="65" data-step="1" data-max="100" data-speed="26" data-duration="1690" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">organic spices</div>
															<div class="sc_skills_item sc_skills_style_1 even">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="35" data-step="1" data-max="100" data-speed="31" data-duration="1085" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">gluten free food</div>
															<div class="sc_skills_item sc_skills_style_1 odd">
																<div class="sc_skills_count">
																	<div class="sc_skills_total" data-start="0" data-stop="90" data-step="1" data-max="100" data-speed="20" data-duration="1800" data-ed="%">
																		<span>0%</span>
																	</div>
																</div>
															</div>
															<div class="sc_skills_info">Aromatic Herbs</div>
														</div>
													</div>
												</div>
											</div>
										</section>

										<section class="section_padding_top_100 section_padding_bottom_60 with_border_bottom">
											<div class="container-fluid">
												<h2 class="sc_title sc_title_normal sc_title_center margin_bottom_30">Meet our team</h2>
												<div class="sc_team sc_team_section_1 sc_team_item_style_1">
													<div class="sc_columns_4 sc_columns_indent">
														<!-- Edit lại avatar -->
														<div class="sc_columns_item">
															<div class="sc_team_item sc_team_item_1 odd first">
																<div class="sc_team_item_avatar_wrap">
																	<div class="sc_team_item_avatar ">
																		<img alt="avartar_1.jpg" src="home/images/avatar_1.jpg">
																	</div>
																	<div class="sc_team_item_socials">
																		<ul>
																			<li>
																				<a href="#" class="social_icons social_facebook facebook" target="_blank" title="Facebook">
																					<span class="icon-facebook"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_twitter twitter" target="_blank" title="Twitter">
																					<span class="icon-twitter"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_gplus gplus" target="_blank" title="Google+">
																					<span class="icon-gplus"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_linkedin linkedin" target="_blank" title="LinkedIn">
																					<span class="icon-linkedin"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_dribbble dribbble" target="_blank" title="Dribbble">
																					<span class="icon-dribbble"></span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
																<div class="sc_team_item_title">Dương Dủ Bân</div>
																<div class="sc_team_item_position">Developer</div>
															</div>
														</div>
														<div class="sc_columns_item">
															<div class="sc_team_item sc_team_item_2 even">
																<div class="sc_team_item_avatar_wrap">
																	<div class="sc_team_item_avatar ">
																		<img alt="avartar_2.jpg" src="home/images/avatar_2.jpg">
																	</div>
																	<div class="sc_team_item_socials">
																		<ul>
																			<li>
																				<a href="#" class="social_icons social_facebook facebook" target="_blank" title="Facebook">
																					<span class="icon-facebook"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_twitter twitter" target="_blank" title="Twitter">
																					<span class="icon-twitter"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_gplus gplus" target="_blank" title="Google+">
																					<span class="icon-gplus"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_linkedin linkedin" target="_blank" title="LinkedIn">
																					<span class="icon-linkedin"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_dribbble dribbble" target="_blank" title="Dribbble">
																					<span class="icon-dribbble"></span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
																<div class="sc_team_item_title">Hoàng Đình Sơn Dương</div>
																<div class="sc_team_item_position">Developer</div>
															</div>
														</div>
														<div class="sc_columns_item">
															<div class="sc_team_item sc_team_item_3 odd">
																<div class="sc_team_item_avatar_wrap">
																	<div class="sc_team_item_avatar ">
																		<img alt="avartar_3.jpg" src="home/images/avatar_3.jpg">
																	</div>
																	<div class="sc_team_item_socials">
																		<ul>
																			<li>
																				<a href="#" class="social_icons social_facebook facebook" target="_blank" title="Facebook">
																					<span class="icon-facebook"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_twitter twitter" target="_blank" title="Twitter">
																					<span class="icon-twitter"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_gplus gplus" target="_blank" title="Google+">
																					<span class="icon-gplus"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_linkedin linkedin" target="_blank" title="LinkedIn">
																					<span class="icon-linkedin"></span>
																				</a>
																			</li>
																			<li>
																				<a href="#" class="social_icons social_dribbble dribbble" target="_blank" title="Dribbble">
																					<span class="icon-dribbble"></span>
																				</a>
																			</li>
																		</ul>
																	</div>
																</div>
																<div class="sc_team_item_title">Nguyễn Chí Bồng</div>
																<div class="sc_team_item_position">Developer</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</section>
										
										<section class="section_padding_top_90 section_padding_bottom_70 with_border_bottom">
											<div class="container">
												<h3 class="sc_title sc_title_center sc_title_style_1 margin_bottom_50">
												Our partners</h3>
												<div class=" sc_columns  sc_columns_7 sc_columns_indent">
													<div class=" sc_columns_item  sc_columns_item_coun_1 odd first" >
														<div  class="sc_image">
															<a href="#">
																<img  src="home/images/partners_1.png" alt="" />
															</a>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_2 even" >
														<div  class="sc_image">
															<a href="#">
																<img  src="home/images/partners_2.png" alt="" />
															</a>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_3 odd" >
														<div  class="sc_image">
															<a href="#">
																<img  src="home/images/partners_3.png" alt="" />
															</a>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_4 even" >
														<div  class="sc_image">
															<a href="#">
																<img  src="home/images/partners_4.png" alt="" />
															</a>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_5 odd" >
														<div  class="sc_image">
															<a href="#">
																<img  src="home/images/partners_5.png" alt="" />
															</a>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_6 even" >
														<div  class="sc_image">
															<a href="#">
																<img  src="home/images/partners_6.png" alt="" />
															</a>
														</div>
													</div>
													<div class=" sc_columns_item  sc_columns_item_coun_7 odd" >
														<div  class="sc_image">
															<a href="#">
																<img  src="home/images/partners_7.png" alt="" />
															</a>
														</div>
													</div>
												</div>												
											</div>
										</section>

										<section class="section_padding_top_100">
											<div class="container-fluid">
												<div class="sc_sidebar_selector">
													<div class="sc_columns_4 sc_columns_indent">
														<div class="widget_area">
															<aside class="sc_columns_item widgetWrap widget_recent_posts widget_trex_post">
																<h5 class="title">Eating &#038; Cooking</h5>
																<ul>
																	<li>
																		<a href="#">Gluten-Free Baked Goods</a>
																	</li>
																	<li>
																		<a href="#">Healthy Food Guide</a>
																	</li>
																	<li>
																		<a href="#">Organic & Natural</a>
																	</li>
																	<li>
																		<a href="#">The Farm Story</a>
																	</li>
																	<li>
																		<a href="#">Plum cake</a>
																	</li>
																	<li>
																		<a href="#">GMOs: Your Right to Know</a>
																	</li>
																	<li>
																		<a href="#">John Mackey's Blog</a>
																	</li>
																</ul>
															</aside>
															<aside class="sc_columns_item widgetWrap widget_recent_posts widget_trex_post">
																<h5 class="title">RECENT POSTS</h5>
																<div class="post_item first">
																	<div class="post_thumb image_wrapper">
																		<img alt="home/Gluten-Free Baked Goods" src="home/images/small_1.jpg">
																	</div>
																	<div class="post_wrapper">
																		<div class="post_title theme_title title_padding">
																			<a href="#">Gluten-Free Baked Goods</a>
																		</div>
																		<div class="post_info theme_info">
																			<span class="post_date theme_text">January 22, 2015</span>
																			<span class="post_author">
																				by
																				<a href="#">trx_user</a>
																			</span>
																		</div>
																	</div>
																</div>
																<div class="post_item">
																	<div class="post_thumb image_wrapper">
																		<img alt="home/Healthy Food Guide" src="home/images/small_2.jpg">
																	</div>
																	<div class="post_wrapper">
																		<div class="post_title theme_title title_padding">
																			<a href="#">Healthy Food Guide</a>
																		</div>
																		<div class="post_info theme_info">
																			<span class="post_date theme_text">January 22, 2015</span>
																			<span class="post_author">
																				by
																				<a href="#">trx_user</a>
																			</span>
																		</div>
																	</div>
																</div>
																<div class="post_item">
																	<div class="post_thumb image_wrapper">
																		<img alt="home/Organic &#038; Natural" src="home/images/small_3.jpg">
																	</div>
																	<div class="post_wrapper">
																		<div class="post_title theme_title title_padding">
																			<a href="#">Organic & Natural</a>
																		</div>
																		<div class="post_info theme_info">
																			<span class="post_date theme_text">January 22, 2015</span>
																			<span class="post_author">
																				by
																				<a href="#">trx_user</a>
																			</span>
																		</div>
																	</div>
																</div>
															</aside>
															<aside class="sc_columns_item widgetWrap widget_recent_posts widget_trex_post">
																<h5 class="title">Blogs</h5>
																<ul>
																	<li>
																		<a href="#">Gluten-Free Baked Goods</a>
																	</li>
																	<li>
																		<a href="#">Healthy Food Guide</a>
																	</li>
																	<li>
																		<a href="#">Organic & Natural</a>
																	</li>
																	<li>
																		<a href="#">The Farm Story</a>
																	</li>
																	<li>
																		<a href="#">Plum cake</a>
																	</li>
																	<li>
																		<a href="#">GMOs: Your Right to Know</a>
																	</li>
																	<li>
																		<a href="#">John Mackey's Blog</a>
																	</li>
																</ul>
															</aside>
															<aside class="sc_columns_item widgetWrap widget_tag_cloud">
																<h5 class="title">Tags</h5>
																<div class="tagcloud">
																	<a href='#' title='1 topic'>Audio</a>
																	<a href='#' title='4 topics'>Brand</a>
																	<a href='#' title='1 topic'>Brands</a>
																	<a href='#' title='8 topics'>Food</a>
																	<a href='#' title='3 topics'>Fresh</a>
																	<a href='#' title='7 topics'>Lifestyle</a>
																	<a href='#' title='1 topic'>New</a>
																	<a href='#' title='12 topics'>Organic</a>
																	<a href='#' title='1 topic'>People</a>
																	<a href='#' title='1 topic'>Photo</a>
																	<a href='#' title='8 topics'>Products</a>
																</div>
															</aside>
														</div>
													</div>
												</div>
											</div>
										</section>

									</div>
								</div>
							</article>
						</section>
					</div>
				</div>
			</div>

@endsection

@section('sidebar')
@endsection

@section('footer')

			<footer class="footerWidget">
				<div class="copyright">
					<a href='http://themerex.net'>ThemeREX</a>
					&copy; 2016 All Rights Reserved.Terms of Use and Privacy Policy.
				</div>
				<div class="custom_footer">
					<div class="text">Join to Healthy Farm</div>
					<div class="sc_emailer aligncenter margin_top_20">
						<form class="sc_emailer_form">
							<input type="text" class="sc_emailer_input" name="email" value="" placeholder="ENTER YOUR EMAIL ADDRESS">
							<a href="#" class="sc_emailer_button icon icon-mail8" title="Submit" data-group="E-mailer subscription"></a>
						</form>
					</div>
				</div>
			</footer>

@endsection

@section('script')

@endsection