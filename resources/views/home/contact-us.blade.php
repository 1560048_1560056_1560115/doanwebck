@extends('home.layouts.master')

@section('content')

			<div class="wrapContent">
				<div id="wrapWide" class="wrapWide">
					<div class="content">
						<section class="singlePage emptyPostFormatIcon emptyPostTitle emptyPostInfo page">
							<article class="postContent">
								<div class="postTextArea">
									
									<section class="section_padding_bottom_50">
										<div class="container">
											<h3 class="sc_title sc_title_style_3 sc_title_center">Contacts</h3>
										</div>
									</section>

									<section class="">
										<div class="container-fluid">
											<div id="sc_googlemap_551" class="sc_googlemap sc_googlemap_style_2" data-address="San Francisco, CA 94102, US" data-latlng="" data-description="San Francisco, CA 94102, US" data-zoom="12" data-style="royal" data-scroll="no" data-point=""></div>
										</div>
									</section>

									<section class="">
										<div class="container">
											<div class="sc_content mainWrap">
												<div class="sc_section sc_section_style_2 sc_align_center sc_columns_2_3">
													<div class="sc_columns  sc_columns_3 sc_columns_indent">
														<div class="sc_columns_item  sc_columns_item_coun_1 odd first" >
															<h5 class="sc_title sc_title_style_3 margin_top_50 margin_bottom_20">Contact Info</h5>
															<div class="sc_contact_info" >
																<div class="sc_contact_info_wrap">
																	<div class="sc_contact_info_item sc_contact_address_1">
																		<div class="sc_contact_info_lable">Address:</div>
																		121 Cypress Hills Street, Apt. #8A, New York, NY, 10020
																	</div>
																	<div class="sc_contact_info_item sc_contact_phone_1">
																		<div class="sc_contact_info_lable">Phone:</div>
																		+1 35 42 56 38 96
																	</div>
																	<div class="sc_contact_info_item sc_contact_website">
																		<div class="sc_contact_info_lable">Website:</div>
																		www.yoursite.com
																	</div>
																	<div class="sc_contact_info_item sc_contact_email">
																		<div class="sc_contact_info_lable">Email:</div>
																		info@yoursite.com
																	</div>
																</div>
															</div>
														</div>
														<div class=" sc_columns_item  sc_columns_item_coun_2 colspan_2 even" >
															<h5 class="sc_title sc_title_style_3 margin_top_50 margin_bottom_20">Leave a message</h5>
															<div class="sc_form contact_form_1">
															    <form class="contact_1" method="post" action="include/contact-form.php">
																	<div class="sc_columns_3 sc_columns_indent">
																		<div class="sc_columns_item sc_form_username">
																			<label class="required" for="contact_form_username">Name</label>
															                <input type="text" name="name" id="contact_form_username">
															            </div>
																		<div class="sc_columns_item sc_form_email">
																			<label class="required" for="contact_form_email">E-mail</label>
															                <input type="text" name="email" id="contact_form_email">
															            </div>
																		<div class="sc_columns_item sc_form_subj">
																			<label class="required" for="contact_form_subj">Subject</label>
															                <input type="text" name="subject" id="contact_form_subj">
															            </div>
															        </div>
																	<div class="sc_form_message">
																		<label class="required" for="contact_form_message">Your Message</label>
														                <textarea  id="contact_form_message" class="textAreaSize" name="message"></textarea>
															        </div>
																	<div class="sc_form_button">
																		<div class="sc_button sc_button_style_3 sc_button_skin_dark sc_button_style_bg sc_button_size_medium">
																			<button type="submit" name="contact_submit" class="contact_form_submit">submit</button>
																		</div>
																	</div>

															        <div class="result sc_infobox"></div>
															    </form>
															</div> 
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>

								</div>
							</article>
						</section>
					</div>
				</div>
			</div>
@endsection

@section('sidebar')
@endsection

@section('script')
	<script type='text/javascript' src='home/js/custom/_form_contact.js'></script>
	<script type='text/javascript' src='http://maps.google.com/maps/api/js'></script>
	<script type='text/javascript' src='home/js/custom/_googlemap_init.js'></script>
@endsection