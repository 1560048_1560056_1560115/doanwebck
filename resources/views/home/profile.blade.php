@extends('home.layouts.master')
@section('css')
    <link rel='stylesheet' href='home/js/vendor/revslider/rs-plugin/css/settings.css' type='text/css' media='all' />
@endsection

@section('content')

            <div class="buttonScrollUp upToScroll icon-up-open-micro"></div>

            <div id="user-popUp" class="user-popUp mfp-with-anim">
                <h1 class="sc_title sc_title_style_3 sc_title_center margin_bottom_10">Profile</h1>
                <div class="sc_line sc_line_style_solid margin_top_30 margin_bottom_30"></div>
                <div class="sc_tabs_array">
                    <div id="registerForm" class="formItems registerFormBody sc_columns_2">
                        <form name="register_form" method="post" class="formValid" enctype="multipart/form-data" action="{{ action('ShopController@postProfileEdit',[$info[0]->id]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="redirect_to" value="#"/>
                            <div>
                                <ul class="formList">
                                <h5 class="sc_title sc_title_style_3 sc_columns_2 margin_bottom_10">Name</h5>
                                    <li class="formUser"><input type="text" id="name" name="name"  value="{{$info[0]->name}}" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </li>
                                <h5 class="sc_title sc_title_style_3 sc_columns_2 margin_bottom_10 margin_top_10">Email</h5>
                                    <li class="formLogin"><input type="text" id="email" name="email" value="{{$info[0]->email}}" placeholder="E-mail" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
										@if (session('status'))
										<div class="sc_infobox sc_infobox_style_warning sc_infobox_closeable sc_infobox_horizontal">
										<h4 class="sc_infobox_title">Warning</h4>
										<span class="sc_infobox_line"></span>
										<span class="sc_infobox_content">{{ session('status') }}</span>
										</div>
										@endif
                                    </li>
                                <h5 class="sc_title sc_title_style_3 sc_columns_2 margin_top_10">Avatar</h5>
	                                <input type="file" name="fImage" id="fImage" accept="image/*" onchange="loadImage(event)" value="{{$info[0]->image}}">
	                                <img src="/{{$info[0]->image}}" alt="" id="image" name="image" width=200 height=200 style="display:block">
                                    <input type="hidden" id="type" name="type" value="user">
                                    <button type="submit" class="margin_top_10 sendEnter enter sc_button sc_button_skin_dark sc_button_style_bg sc_button_size_medium">Save</button>
                                </ul>
                            </div>
                            <div class="sc_result result sc_infobox sc_infobox_closeable"></div>
                        </form>
                    </div>
                </div>
            </div>

@endsection

@section('sidebar')
@endsection