					<div class="wrapTopMenu">
						<div class="topMenu main">
							<ul id="mainmenu" class="">
								<li class="current-menu-ancestor">
									<a href="/">Home</a>
								</li>
								<li class="">
									<a href="/about-us">About us</a>
								</li>
								<li class="">
									<a href="/shop">Shop</a>
								</li>
								<li class="menu-item-has-children">
									<a href="#">Categories</a>
									<ul class="sub-menu">
										@foreach($ds_the_loai as $the_loai)
										<li class="">
											<a href="{!! URL::route('category',$the_loai->id) !!}">{{$the_loai->name}}</a>
										</li>
										@endforeach
									</ul>
								</li>
								<li class="">
									<a href="/contact-us">Contact Us</a>
								</li>
							</ul>
						</div>
					</div>