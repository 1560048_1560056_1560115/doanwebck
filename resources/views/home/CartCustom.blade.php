@extends('home.layouts.master')
@section('css')
	<link rel='stylesheet' href="home/js/vendor/woocommerce/css/select2.css" type='text/css' media='all' />
	<link rel='stylesheet' href="home/js/vendor/woocommerce/css/woocommerce-layout.css" type='text/css' media='all' />
	<link rel='stylesheet' href="home/js/vendor/woocommerce/css/woocommerce-smallscreen.css" type='text/css' media='only screen and (max-width: 768px)' />
	<link rel='stylesheet' href="home/js/vendor/woocommerce/css/woocommerce.css" type='text/css' media='all' />
	<link rel='stylesheet' href="home/css/woo-style.css" type='text/css' media='all' />
@endsection

@section('body')
<body class="woocommerce woocommerce-page">

	<div id="wrap" class="wrap sideBarRight sideBarShow menuStyle1 menuSmartScrollShow blogStyleExcerpt bodyStyleWide menuStyleFixed visibleMenuDisplay logoImageStyle logoStyleBG">
@endsection

@section('content')

<div class="wrapContent">
				<div id="wrapWide" class="wrapWide">
					<div class="content">
						<div class="postTextArea">
							<div class="woocommerce">
								<form class="woocommerce-cart-form" action="{{route('updatebuyproduct')}}" method="get">
	
									<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
										<thead>
											<tr>
												<th class="product-remove">&nbsp;</th>
												<th class="product-thumbnail">&nbsp;</th>
												<th class="product-name">Product</th>
												<th class="product-price">Price</th>
												<th class="product-quantity">Quantity</th>
												<th class="product-subtotal">Total</th>
											</tr>
										</thead>
										<tbody>
										@if(!Cart::isEmpty())	
											@foreach(Cart::getContent() as $product)
											<tr class="woocommerce-cart-form__cart-item cart_item">

												
												<td class="product-remove">
													<a href="{{route('remove_buy_product',$product->id)}}" class="remove" aria-label="Remove this item" data-product_id="4012" data-product_sku="">×</a>						
												</td>

												<td class="product-thumbnail">
												<a href="{{route('detail',$product->id)}}"><img width="180" height="180" src="{{$product->attributes->image}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="{{$product->attributes->image}} 180w" sizes="(max-width: 180px) 100vw, 180px"></a>						</td>

												<td class="product-name" data-title="Product">
												<a href="{{route('detail',$product->id)}}">{{$product->name}}</a>						</td>

												<td class="product-price" data-title="Price">
												<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>{{$product->price}}</span>						</td>
												<input type="hidden" name="id{{$product->id}}" value="{{$product->id}}" />

												<td class="product-quantity" data-title="Quantity">
													<div class="quantity">
														<label class="screen-reader-text" for="quantity_5a3be99669e96">Quantity</label>
														<input type="number" name="quantity{{$product->id}}" class="input-text qty text" step="1" min="0" max=""  value="{{$product->quantity}}" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric">
													</div>
												</td>

												<td class="product-subtotal" data-title="Total">
													<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>{{$product->getPriceSum()}}</span>						</td>
												
											</tr>
											@endforeach
										@endif
			
											<tr>
												<td colspan="6" class="actions">

													<div class="coupon">
														<label for="coupon_code">Coupon:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code"> <input type="submit" class="button" name="apply_coupon" value="Apply coupon">
													</div>
												
												<input type="submit" class="button" name="update_cart" value="Update cart" >

					
												<input type="hidden" id="_wpnonce" name="_wpnonce" value="e96f5a371a"><input type="hidden" name="_wp_http_referer" value="/?page_id=3965">				</td>
											</tr>

									</tbody>
							</table>
						</form>

						<div class="cart-collaterals">
							<div class="cart_totals ">

	
								<h2>Cart totals</h2>

								<table cellspacing="0" class="shop_table shop_table_responsive">

									<tbody><tr class="cart-subtotal">
										<th>Subtotal</th>
										<td data-title="Subtotal"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>{{Cart::getSubTotal()}}</span></td>
									</tr>

		
		
		
		
		
									<tr class="order-total">
										<th>Total</th>
										<td data-title="Total"><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>{{Cart::getTotal()}}</span></strong> </td>
									</tr>

		
									</tbody>
								</table>

					<div class="wc-proceed-to-checkout">
		
				<a href="{{route('getcheckout')}}" class="checkout-button button alt wc-forward">
					Proceed to checkout</a>
				</div>

	
			</div>
		</div>

	</div>
 
	</div>		
	</div>	

@endsection