@extends('home.layouts.master')
@section('css')
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/select2.css" type='text/css' media='all' />
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/woocommerce-layout.css" type='text/css' media='all' />
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/woocommerce-smallscreen.css" type='text/css' media='only screen and (max-width: 768px)' />
	<link rel='stylesheet' href="/home/js/vendor/woocommerce/css/woocommerce.css" type='text/css' media='all' />
	<link rel='stylesheet' href="/home/css/woo-style.css" type='text/css' media='all' />
@endsection

@section('body')
<body class="woocommerce woocommerce-page">

	<div id="wrap" class="wrap sideBarRight sideBarShow menuStyle1 menuSmartScrollShow blogStyleExcerpt bodyStyleWide menuStyleFixed visibleMenuDisplay logoImageStyle logoStyleBG">
@endsection

@section('menu_cart')
							<li class="usermenuCart">
								<a href="#" class="cart_button">
									<img src="/home/images/icon-cart.png" alt="">
								</a>
								<ul class="widget_area sidebar_cart sidebar">
									<li>
										<div class="widget woocommerce widget_shopping_cart">
											<div class="hide_cart_widget_if_empty">
												<div class="widget_shopping_cart_content">
													<ul class="cart_list product_list_widget">
														<li class="empty">No products in the cart.</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>
@endsection

@section('content')

			<div class="wrapContent">
				<div id="wrapWide" class="wrapWide">
					<div class="content">

						<section class="">
							<div class="container">
								<section class="post shop_mode_thumbs">
									<article class="post_content">
										<nav class="woocommerce-breadcrumb" >
											<a href="/">Home</a>&nbsp;&#47;&nbsp;Category&nbsp;&#47;&nbsp;{{$the_loai[0]->name}}
										</nav>
										<h1 class="page-title">Category : {{$the_loai[0]->name}}</h1>
										<p class="woocommerce-result-count"></p>
										<form class="woocommerce-ordering" method="get">
											<select name="orderby" class="orderby">
												<option value="menu_order"  selected='selected'>Default sorting</option>
												<option value="popularity" >Sort by popularity</option>
												<option value="rating" >Sort by average rating</option>
												<option value="date" >Sort by newness</option>
												<option value="price" >Sort by price: low to high</option>
												<option value="price-desc" >Sort by price: high to low</option>
											</select>
											<input type="hidden" name="post_type" value="product" />
										</form>
										<ul class="products">
											@foreach($ds_sanpham as $sanpham)
								
											@if ($sanpham->id%4 == 0)
											<li class="last product">
											@else
											<li class="product">
											@endif
												<a href="{!! URL::route('detail',$sanpham->id) !!}">
													@if ($sanpham->on_sale == 1)
													<span class="onsale">Sale!</span>
													@endif
													<img src="/{{$sanpham->image}}" class="attachment-shop_catalog" alt="apetito1" />
													<h3>{{$sanpham->name_product}}</h3>
													<span class="price">
														<del>
															<span class="amount">&pound;15.00</span>
														</del>
														<ins>
															<span class="amount">&pound;{{$sanpham->price}}</span>
														</ins>
													</span>
												</a>
												<a href="#" class="button add_to_cart_button product_type_simple">Add to cart</a>
											</li>
											@endforeach
										</ul>
										<nav class="woocommerce-pagination">
											<ul class='page-numbers'>
												<li>
													<span class='page-numbers'>{{ $ds_sanpham->links() }}</span>
												</li>
											</ul>
										</nav>
									</article>
								</section>
							</div>
						</section>

					</div>

@endsection
