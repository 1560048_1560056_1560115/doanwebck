@extends('admin.layouts.master')
    @section('Main')


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>Edit</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (count($errors) >0)
                                    
                                        <ul>
                                        @foreach($errors->all() as $error)
                                            <li class="text-danger" > {{ $error }}</li>
                                        @endforeach
                                        </ul>
                                    
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('user_postedit')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Email</label>
                                <input type="hidden" name="id" value="{{$user->id}}" />
                                <input type="email" class="form-control" name="txtEmail" value="{{$user->email}}" disabled />
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="txtPass" placeholder="Please Enter Password" />
                            </div>
                            <div class="form-group">
                                <label>RePassword</label>
                                <input type="password" class="form-control" name="txtRePass" placeholder="Please Enter RePassword" />
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input  class="form-control" name="txtUserName" placeholder="Please User Name" />
                            </div>
                            <div class="form-group">
                                <label>User Level</label>
                                <label class="radio-inline">
                                   
                                    <input name="rdoLevel" value="admin"  @if($user->type == 'admin') {{'checked=""'}} @endif type="radio">Admin
                                    
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoLevel" value="user" @if($user->type == 'user') {{'checked=""'}} @endif  type="radio">User
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">User Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@stop