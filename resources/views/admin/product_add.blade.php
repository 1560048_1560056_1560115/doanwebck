@extends('admin.layouts.master')
    @section('Main')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('post_product')}}" method="POST" enctype="multipart/form-data" onsubmit="return checkProductAdd()">
                        <input type="hidden" = name="_token" value = "{!!csrf_token()!!}">
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" name="name" id="name"/>
                            </div>
                            <div class="form-group error-msg" id="div-error-name">
                                  <i class="fa fa-times-circle" id="error-name"></i>
                            </div>
                            @if($errors->has('name'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('name')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Category</label>
                                <select class="form-control" name="category" id="category">
                                @foreach($cates as $cate)
                                <option value="{{$cate->id}}">{{$cate->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group error-msg" id="div-error-cate">
                                  <i class="fa fa-times-circle" id="error-category"></i>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input class="form-control" type="number" name="price" id="price" />
                            </div>
                            <div class="form-group error-msg" id="div-error-price">
                                  <i class="fa fa-times-circle" id="error-price"></i>
                            </div>
                            @if($errors->has('price'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('price')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Product Onsale: </label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" type="radio" id="rdoStatusYes" value="1"> Yes
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" type="radio" id="rdoStatusNo" value="0"> No
                                </label>
                            </div>
                            <div class="form-group error-msg" id="div-error-status">
                                  <i class="fa fa-times-circle" id="error-status"></i>
                            </div>
                            @if($errors->has('rdoStatus'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('rdoStatus')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="3" name="des" id="des"></textarea>
                            </div>
                            <div class="form-group error-msg" id="div-error-des">
                                  <i class="fa fa-times-circle" id="error-des"></i>
                            </div>
                            @if($errors->has('des'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('des')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Images</label>
                                <input type="file" name="fImage" id="fImage" accept="image/*" onchange="loadImage(event)">
                                <img src="" alt="" id="image" name="image">
                            </div>
                            <div class="form-group error-msg" id="div-error-image">
                                  <i class="fa fa-times-circle" id="error-image"></i>
                            </div>
                            @if($errors->has('fImage'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('fImage')}}</i>
                                </div>
                            @endif
                            <input type="hidden" id="view" name="view" value="0">
                            <button type="submit" class="btn btn-default" >Add Product</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

  @stop