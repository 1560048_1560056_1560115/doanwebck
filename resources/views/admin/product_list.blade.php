@extends('admin.layouts.master')
    @section('Main')


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Description</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ds_sanpham as $sanpham)
                            <tr class="even gradeC" align="center">
                                <td>{{$sanpham->id}}</td>
                                <td>{{$sanpham->name_product}}</td>
                                <td>{{$sanpham->price}}</td>
                                <td>{{$sanpham->on_sale}}</td>
                                <td>{{$sanpham->description}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="{!!URL::route('deleteProduct',$sanpham->id)!!}">Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!!URL::route('idProduct',$sanpham->id)!!}">Edit</a></td>
                            </tr>
                            @endforeach   
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->



@stop
