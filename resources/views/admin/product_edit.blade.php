@extends('admin.layouts.master')
    @section('Main')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>Edit</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{ action('AdminController@postProductEdit',[$sanpham[0]->id]) }}" method="POST"  enctype="multipart/form-data" onsubmit="return checkProductEdit()">
                        <input type="hidden" = name="_token" value = "{!!csrf_token()!!}">
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" id="name" name="name" value="{{$sanpham[0]->name_product}}" />
                            </div>
                            <div class="form-group error-msg" id="div-error-name">
                                  <i class="fa fa-times-circle" id="error-name"></i>
                            </div>
                            @if($errors->has('name'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('name')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Category</label>
                                <select class="form-control" name="category" id="category">
                                    <option value="{{$current[0]->id}}">{{$current[0]->name}}</option>
                                    @foreach($cates as $cate)
                                    @if($current[0]->id != $cate->id )
                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group error-msg" id="div-error-cate">
                                  <i class="fa fa-times-circle" id="error-category"></i>
                            </div>
                            @if($errors->has('category'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('category')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Price</label>
                                <input class="form-control" id="price" name="price" type="number" value="{{$sanpham[0]->price}}" />
                            </div>
                            <div class="form-group error-msg" id="div-error-price">
                                  <i class="fa fa-times-circle" id="error-price"></i>
                            </div>
                            @if($errors->has('price'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('price')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Product Onsale: </label>
                                @if ($sanpham[0]->on_sale == 1)
                                    <label class="radio-inline">
                                        <input name="rdoStatus" type="radio" id="rdoStatusYes" value="1" checked="checked"> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input name="rdoStatus" type="radio" id="rdoStatusNo" value="0"> No
                                    </label>
                                @else
                                    <label class="radio-inline">
                                        <input name="rdoStatus" type="radio" id="rdoStatusYes" value="1"> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input name="rdoStatus" type="radio" id="rdoStatusNo" value="0" checked="checked"> No
                                    </label>
                                @endif
                            </div>
                            <div class="form-group error-msg" id="div-error-status">
                                  <i class="fa fa-times-circle" id="error-status"></i>
                            </div>
                            @if($errors->has('status'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('status')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" id="des" rows="3" name="des">{{$sanpham[0]->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>View</label>
                                <textarea class="form-control" id="view" rows="3" name="view">{{$sanpham[0]->views_number}}</textarea>
                            </div>
                            <div class="form-group error-msg" id="div-error-des">
                                  <i class="fa fa-times-circle" id="error-des"></i>
                            </div>
                            @if($errors->has('des'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('des')}}</i>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Images</label>
                                <input type="file" name="fImage" id="fImage" accept="image/*" onchange="loadImage(event)" value="{{$sanpham[0]->image}}">
                                <img src="/{{$sanpham[0]->image}}" alt="" id="image" name="image">
                            </div>
                            <div class="form-group error-msg" id="div-error-image">
                                  <i class="fa fa-times-circle" id="error-image"></i>
                            </div>
                            @if($errors->has('fImage'))
                                <div class="form-group error-msg" id="div-error" style="display:block">
                                      <i class="fa fa-times-circle" id="error">{{$errors->first('fImage')}}</i>
                                </div>
                            @endif
                            <button type="submit" class="btn btn-default">Product Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@stop