@extends('home.layouts.master')
@section('css')
    <link rel='stylesheet' href='home/js/vendor/revslider/rs-plugin/css/settings.css' type='text/css' media='all' />
@endsection

@section('content')

            <div class="buttonScrollUp upToScroll icon-up-open-micro"></div>

            <div id="user-popUp" class="user-popUp mfp-with-anim">
                <h1 class="sc_title sc_title_style_3 sc_title_center margin_bottom_10">Sign up</h1>
                <div class="sc_line sc_line_style_solid margin_top_30 margin_bottom_30"></div>
                <div class="sc_tabs_array">
                    <div id="registerForm" class="formItems registerFormBody sc_columns_2">
                        <form name="register_form" method="post" class="formValid" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="redirect_to" value="#"/>
                            <div>
                                <ul class="formList">
                                    <li class="formUser"><input type="text" id="name" name="name"  value="{{ old('name') }}" placeholder="User name (login)" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </li>
                                    <li class="formPass"><input type="password" id="password" name="password" value="" placeholder="Password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </li>
                                    <li class="formPass"><input id="password-confirm" type="password" name="password_confirmation" value="" placeholder="Confirm Password" required></li>
                                    <li class="formLogin"><input type="text" id="email" name="email" value="{{ old('email') }}" placeholder="E-mail" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </li>
                                    <input type="hidden" id="type" name="type" value="user">
                                    <input type="hidden" id="image" name="image" value="home/images/user.png">
                                    <li class="i-agree">
                                        <input type="checkbox" value="forever" id="i-agree" name="i-agree">
                                        <label for="i-agree">I agree with</label> <a href="#">Terms & Conditionst</a>
                                    </li>
                                    <button type="submit" class="sendEnter enter sc_button sc_button_skin_dark sc_button_style_bg sc_button_size_medium">Sign Up</button>
                                </ul>
                            </div>
                            <div class="sc_result result sc_infobox sc_infobox_closeable"></div>
                        </form>
                    </div>
                </div>
            </div>

@endsection

@section('sidebar')
@endsection