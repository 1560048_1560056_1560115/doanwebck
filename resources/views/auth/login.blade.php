@extends('home.layouts.master')
@section('css')
    <link rel='stylesheet' href='home/js/vendor/revslider/rs-plugin/css/settings.css' type='text/css' media='all' />
@endsection

@section('content')

            <div class="buttonScrollUp upToScroll icon-up-open-micro"></div>

            <div id="user-popUp" class="user-popUp mfp-with-anim">
                <h1 class="sc_title sc_title_style_3 sc_title_center margin_bottom_10">Log in</h1>
                <div class="sc_line sc_line_style_solid margin_top_30 margin_bottom_30"></div>
                <div class="sc_tabs_array">
                    <div id="loginForm" class="formItems loginFormBody sc_columns_2">
                        <form action="#" method="post" name="login_form" class="formValid" action="{{ route('login') }}">
                            {{ csrf_field() }}
                        <div>
                                <input type="hidden" name="redirect_to" value="#" />
                                <ul class="formList">
                                    <li class="formLogin"><input type="text" id="email" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    </li>
                                    <li class="formPass"><input type="password" id="password" name="password" value="" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    </li>
                                    <li class="remember">
                                        <a href="#" class="forgotPwd">Forgot password?</a>
                                        <input type="checkbox" value="forever" id="rememberme" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="rememberme">Remember me</label>
                                    </li>
                                    <button type="submit" class="sendEnter enter sc_button sc_button_skin_global sc_button_style_bg sc_button_size_medium">Login</button></li>
                                </ul>
                        </div>

                        <div>
                            <ul class="formList">
                                <li>You can login using your social profile</li>
                                <li class="loginSoc">
                                    <a href="#" class="iconLogin fb icon-facebook"></a>
                                    <a href="#" class="iconLogin tw icon-twitter"></a>
                                    <a href="#" class="iconLogin gg icon-gplus"></a>
                                </li>
                                <li><a href="{{ route('password.request') }}">Problem with login?</a></li>
                            </ul>
                        </div>
                        <div class="sc_result result sc_infobox sc_infobox_closeable"></div>
                        </form>
                    </div>
                </div>
            </div>

@endsection

@section('sidebar')
@endsection