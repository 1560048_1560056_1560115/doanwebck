<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ShopController@home');

Route::prefix('shop')->group(function() {
	Route::get('/',['as' => 'shop','uses'=>'ShopController@shop']);
	Route::get('/product/{id}',['as' => 'detail','uses' => 'ShopController@shop_product']);
	Route::post('/product/{id}','ShopController@postCommentGuest')->name('post_comment_guest');
	Route::post('/product/comment/{id}','ShopController@postCommentUser')->name('post_comment_user');
	Route::get('/category/{id}',['as' => 'category','uses' => 'ShopController@shop_category']);
	Route::get('/search',['as' => 'search','uses' => 'ShopController@Search']);
});

Route::get('/about-us','ShopController@aboutus');

Route::get('/contact-us','ShopController@contactus');

Route::get('/profile/{id}','ShopController@profile')->name('profile');

Route::post('/profile/{id}','ShopController@postProfileEdit')->name('profile');

Route::get('/user/changepass/{id}','ShopController@changePass')->name('changePass');

Route::post('/user/changepass/{id}','ShopController@postChangePass');

Route::group(['middleware' => ['admin']],function() {

	Route::prefix('admin')->group(function() {

		Route::get('/cate/list','AdminController@getCateList');

		Route::get('/home','AdminController@getHome');

		Route::get('/cate/add','AdminController@getCateAdd');

		Route::get('/cate/edit','AdminController@getCateEdit');

		Route::get('/product/list',['as'=>'admin.list','uses'=>'AdminController@getProductList']);

		Route::get('/product/add','AdminController@getProductAdd');

		Route::post('/product/add','AdminController@postProductAdd')->name('post_product');

		Route::get('/product/edit/{id}',['as'=>'idProduct','uses'=>'AdminController@getProductEdit']);

		Route::post('/product/edit/{id}',['as'=>'idProduct','uses'=>'AdminController@postProductEdit']);

		Route::get('/product/delete/{id}',['as'=>'deleteProduct','uses'=>'AdminController@getDeleteProduct']);

		Route::get('/user/list',['as'=>'user_list','uses'=>'AdminController@getUserList']);

		Route::get('/user/add','AdminController@getUserAdd');

		Route::get('/user/edit/{id}',['as'=>'user_edit','uses'=>'AdminController@getUserEdit']);	

		Route::post('/user/post_edit',['as'=>'user_postedit','uses'=>'AdminController@postUserEdit']);

		Route::get('/user/delete/{id}',['as'=>'user_getdelete','uses'=>'AdminController@getUserDelete']);

		Route::post('/user/add/',['as'=>'user_postadd','uses'=>'AdminController@postUserAdd']);
	});
});

Auth::routes();

Route::get('/logout',['as'=>'getlogout','uses'=>'ShopController@getlogout']);
Route::get('/abc', 'HomeController@index')->name('home');



Route::get('/buy_product/{id}',['as'=>'buy_product','uses'=>'ShopController@buyProduct']);
Route::get('/remove_buy_product/{id}',['as'=>'remove_buy_product','uses'=>'ShopController@removeBuyProduct']);
Route::get('/cartcustom',['as'=>'cartcustom','uses'=>'ShopController@getCartCustom']);
Route::get('/getbuyproduct/{id}',['as'=>'getbuyproduct','uses'=>'ShopController@getbuyProduct']);
Route::get('/updatebuyproduct',['as'=>'updatebuyproduct','uses'=>'ShopController@getUpdateBuyProduct']);
route::post('/postcheckout',['as'=>'postcheckout','uses'=>'ShopController@postCheckOut']);

Route::get('/checkout',['as'=>'getcheckout','uses'=>'ShopController@getCheckOut']);

route::post('/saveCheckout',['as'=>'savecheckout','uses'=>'ShopController@postSaveCheckOut']);
Route::get('/savebillsuccess',['as'=>'savebillsuccess','uses'=>'ShopController@getSaveBillSuccess']);